package com.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dao.UserDao;
import com.model.User;

@Service
public class UserServiceImpl implements UserService{
	
	ArrayList<User> al = new ArrayList<>();
	
	@Autowired
	private UserDao userDao;

	@Override
	public void addUser(User user) {
		// TODO Auto-generated method stub
//		al.add(user);
		userDao.save(user);
		System.out.println(al);
	}

	@Override
	public List<User> loadUsers() {
		// TODO Auto-generated method stub
		List<User> lst = (List<User>) userDao.findAll();
		return lst;
//		return al;
	}

	@Override
	public boolean findUser(String name) {
//		for(User usr:al) {
//			if(usr.getUname().equals(name)) {
//				return true;
//			}
//		}
//		return false;
		Optional<User> data =userDao.findById(name);
		if(data.isPresent()) {
			return true;
		}
		return false;
	}

	@Override
	public boolean deleteUser(String name) {
//		for(User usr:al) {
//			if(usr.getUname().equals(name)) {
//				al.remove(usr);
//				return true;
//			}
//		}
//		return false;
		Optional<User> data =userDao.findById(name);
		if(data.isPresent()) {
			userDao.deleteById(name);
			return true;
		}
		return false;
	}

	@Override
	public void updateuser(String name, User user) {
		// TODO Auto-generated method stub
		Optional<User> data =userDao.findById(name);
		if(data.isPresent()) {
			userDao.deleteById(name);
			userDao.save(user);
		}
		
	}

}
