package com.service;

import java.util.List;

import com.model.User;

public interface UserService {

	public void addUser(User user);
	
	public List<User> loadUsers();
	
	public boolean findUser(String name);
	
	public boolean deleteUser(String name);
	
	public void updateuser(String name, User user);
}
