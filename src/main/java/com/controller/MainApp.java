package com.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.model.User;
import com.service.UserService;

@RestController
@RequestMapping("/mainapp")
public class MainApp {
	
//	@Value("${spring.application.name}")
	public String name;
	
	@GetMapping("/")
	public String name() {
		return name;
	}
	
	@Autowired
	private UserService service;
	
	@PostMapping("/register")
//	public String registerUser(@ModelAttribute User user) {
	public String registerUser(@RequestBody User user) {
		
//		al.add(user);
		service.addUser(user);
//		System.out.println(al);
//		return "register";
		return "user registered";
	}

	
	@GetMapping("/loadusers")
//	@ResponseBody - we remove this when using @RestController
	public List<User> loadUsers(){
		return service.loadUsers();
	}
	
	@GetMapping("/finduser/{name}")
	public String findUser(@PathVariable String name) {
		if(service.findUser(name)) {
			return name+" found";
		}
		return name+" | user not  found";
	}
	
	@DeleteMapping("/deleteuser/{name}")
	public String deleteUser(@PathVariable String name) {
		if(service.deleteUser(name)) {
			return name+" deleted";
		}
		return name+" | user not  found";
	}
	
	@PutMapping("/updateuser/{name}")
	public String updateUser(@PathVariable String name, @RequestBody User user) {
		service.updateuser(name, user);
		//		service.updat/deleteuser/{name}eUser(name, user);
		return "user updated";
	}
	


}
