package com.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;


@Controller
@RequestMapping("/mainapp")
public class ResController {
	
	@GetMapping("/login")
	public String login() {
		return "login";
	}
	
	@GetMapping("/register")
	public String register() {
		return "register";
	}
	
//	@RequestMapping(value = "/register", method = RequestMethod.POST)
//	public String registerUser(@RequestParam("uname") String uname, @RequestParam("password") String password,
//			@RequestParam("email") String email,@RequestParam("city") String city) {
//		
//		al.add(new User(uname,password, email, city));
//		System.out.println(al);
//		return "register";
//	}
	


}
